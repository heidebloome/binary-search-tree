// REFS
const InsertBtnEl = document.querySelector('button[data-action="insert"]');
const InsertInputEl = document.querySelector('#insert-input');
const FindBtnEl = document.querySelector('button[data-action="find"]');
const FindInputEl = document.querySelector('#find-input');
const DeleteBtnEl = document.querySelector('button[data-action="delete"]');
const DeleteInputEl = document.querySelector('#delete-input');

// INPUTS VALUES
let insertValue = '';
let findValue = '';
let deleteValue = '';

// BSTNode and BST CLASSES
class BinarySearchTreeNode {
  constructor(value = null) {
    this.left = null;
    this.right = null;
    this.parent = null;
    this.value = value;
  };

  insert(value) {
    if (value < this.value) {
      if(this.left) return this.left.insert(value);

      let newBSTNode = new BinarySearchTreeNode(value);
      this.setLeft(newBSTNode);
      return newBSTNode;
    };

    if(value > this.value) {
      if(this.right) return this.right.insert(value);

      let newBSTNode = new BinarySearchTreeNode(value);
      this.setRight(newBSTNode);
      return newBSTNode;
    };

    if(value === this.value) {
      console.log(`There is already a node with this value: ${value}`);
      return;
    }
  };

  find(value) {
    if(value === this.value) {
      return this;
    };

    if(value < this.value && this.left) {
      return this.left.find(value);
    };

    if(value > this.value && this.right) {
      return this.right.find(value);
    };

    return null;
  };

  setLeft(node) {
    this.left = node;
    this.left.parent = this;
  };

  setRight(node) {
    this.right = node;
    this.right.parent = this;
  };

  findMin() {
    if (!this.left) {
      return this;
    };

    return this.left.findMin();
  };

  removeChild(nodeToRemove) {
    if (this.left && this.left === nodeToRemove) {
      this.left = null;
      return true;
    };

    if (this.right && this.right === nodeToRemove) {
      this.right = null;
      return true;
    };

    return false;
  };

  replaceChild(nodeToReplace, replacementNode) {
    if (!nodeToReplace || !replacementNode) {
      return false;
    };

    if (this.left && this.left === nodeToReplace) {
      this.left = replacementNode;
      return true;
    };

    if (this.right && this.right === nodeToReplace) {
      this.right = replacementNode;
      return true;
    };

    return false;
  }
};

class BinarySearchTree {
  constructor() {
    this.root = new BinarySearchTreeNode();
  };

  insert(value) {
    if (!this.root.value) {
      this.root.value = value;
      return;
    }
    return this.root.insert(value);
  };

  find(value) {
    return this.root.find(value);
  };

  remove(value) {
    const nodeToRemove = this.find(value);

    if (!nodeToRemove) {
      console.log(`There is no nodes in this BST with value ${value}`);
      return;
    };

    const parent = nodeToRemove.parent;

    if (!nodeToRemove.left && !nodeToRemove.right) {
      if (parent) {
        parent.removeChild(nodeToRemove);
      } else {
        nodeToRemove.value = null;
      };
    } else if (nodeToRemove.left && nodeToRemove.right) {
      const nextBiggerNode = nodeToRemove.right.findMin();

      if (nextBiggerNode.value === nodeToRemove.right.value) {
        nodeToRemove.value = nodeToRemove.right.value;
        if (nextBiggerNode.right?.right) {
          nodeToRemove.setRight(nodeToRemove.right.right);
        } else {
          nodeToRemove.right = null;
        }
      } else {
        this.remove(nextBiggerNode.value);
        nodeToRemove.value = nextBiggerNode.value;
      };
    } else {
      const childNode = nodeToRemove.left || nodeToRemove.right;

      if (parent) {
        parent.replaceChild(nodeToRemove, childNode);
      } else {
        this.root = childNode;
      };
    };

    nodeToRemove.parent = null;
    return true;
  };
};

// STARTER ARRAY TO BUILD A BTS
const array = [39, 27, 58, 492, 44, 5, 29, 85, 2, 6];
const BST = new BinarySearchTree();
array.map(item => {
  BST.insert(item);
});
console.log('BST:', BST);

// EVENT LISTENERS
InsertInputEl.addEventListener('input', () => {
  insertValue = Number(InsertInputEl.value);
});

InsertBtnEl.addEventListener('click', () => {
  if (insertValue === '') {
    console.log('Enter a value...');
    return;
  }
  const nodeToInsert = BST.insert(insertValue);
  InsertInputEl.value = '';
  insertValue = '';

  if (nodeToInsert === undefined) {
    return;
  };

  console.log(`A new node with value ${insertValue} was added to BST:`, nodeToInsert);
  console.log('UPDATED BST:', BST);
});

FindInputEl.addEventListener('input', () => {
  findValue = Number(FindInputEl.value);
});

FindBtnEl.addEventListener('click', () => {
  if (findValue === '') {
    console.log('Enter a value...');
    return;
  }

  const nodeToFind = BST.find(findValue);
  FindInputEl.value = '';

  if (nodeToFind === null) {
    console.log(`There is no nodes in this BST with this value: ${findValue}`);
    findValue = '';
    return;
  }

  console.log(`A node with value ${findValue}:`, nodeToFind);
  
  findValue = '';
});

DeleteInputEl.addEventListener('input', () => {
  deleteValue = Number(DeleteInputEl.value);
});

DeleteBtnEl.addEventListener('click', () => {
  if (deleteValue === '') {
    console.log('Enter a value...');
    return;
  }

  BST.remove(deleteValue);
  DeleteInputEl.value = '';

  console.log(`A node with value ${deleteValue} was deleted.`);
  console.log('UPDATED BST:', BST);

  deleteValue = '';
});